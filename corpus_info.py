import argparse
import json
import sys
import argparse
import os.path
import xlsxwriter
import glob

parser = argparse.ArgumentParser(description='Given a folder full of middles_tool output files, this will output an'
    ' excel doc with info about how many sentences are in each file.',
                                 prog="corpus_info.py")
parser.add_argument("dir", type=str, help="The path to folder")
parser.add_argument("excel", type=str, help="The path of the excel document that will be created")

args = parser.parse_args()
print(args.dir)
print(args.excel)

#no matter what the excel file name is, it will have the right file ending.
args.excel = os.path.splitext(args.excel)[0] + ".xlsx"

# print(os.path.splitext(args.excel)[1])
# print(os.path.splitext(args.excel)[0])

workbook = xlsxwriter.Workbook(args.excel)
worksheet = workbook.add_worksheet()


file_count = 0
total_sentences = 0
i = 2
for file_path in glob.iglob(args.dir + '*'):
    print(file_path, flush = True)
    file = open(file_path, 'r')
    file_json = json.load(file)
    worksheet.write(i, 0, os.path.basename(file.name))
    worksheet.write(i, 1, file_json['sentence_number'])
    i += 1
    file_count+= 1
    total_sentences += file_json['sentence_number']
worksheet.write(0, 0, total_sentences)
worksheet.write(0, 1, file_count)
worksheet.write(1,0, total_sentences/file_count)



workbook.close()