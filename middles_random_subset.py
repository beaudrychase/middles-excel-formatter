import argparse
import json
import sys
import argparse
import os.path
import xlsxwriter
import glob
import os
from random import sample

parser = argparse.ArgumentParser(description='Given a directory full of parser output files, this program will create a new'
	' directory with with the same files but with only a random subset of sentences.',
                                 prog="middles_random_subset.py")
parser.add_argument("dir", type=str, help="The directory that has the input files")
parser.add_argument("new_dir", type=str, help="The path of the new directory")
parser.add_argument("choose", type=int, help="the minimum number of sentences that are to be chosen from each file.")
args = parser.parse_args()
print(args.dir)
print(args.new_dir)
print(args.choose)

#no matter what the excel file name is, it will have the right file ending.

# print(os.path.splitext(args.excel)[1])
# print(os.path.splitext(args.excel)[0])
assert args.choose >= 0;
try:
	if not os.path.isdir(args.new_dir):
		os.mkdir(args.new_dir)
except:
	print("Creation of directory %s failed" % args.new_dir)
	exit()



for file_path in glob.iglob(args.dir + '*'):
	print(file_path, flush = True)
	file = open(file_path, 'r')
	file_json = json.load(file)
	if file_json['sentence_number'] > args.choose:
		file_json['sentence_number'] = args.choose
		#The number of sentences in the file is greater than the number we want from each file
		#Take a random subset of these sentences.
		file_json['results'] = sample(file_json['results'], args.choose)
		#save the resulting json to the new directory.
	print(file.name)
	f = open(args.new_dir + '/'+ os.path.basename(file.name), "w")
	json.dump(file_json, f)
	f.close()

