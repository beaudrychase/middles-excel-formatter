import argparse
import json
import sys
import argparse
import os.path
import xlsxwriter


parser = argparse.ArgumentParser(description='Given a completed json data file this program will turn it into an excel doc.',
                                 prog="middles_excel_formatter.py")
parser.add_argument("json", type=str, help="The path of the completed json data file.")
parser.add_argument("excel", type=str, help="The path of the excel document that will be created")

args = parser.parse_args()


#no matter what the excel file name is, it will have the right file ending.
args.excel = os.path.splitext(args.excel)[0] + ".xlsx"

print(os.path.splitext(args.excel)[1])
print(os.path.splitext(args.excel)[0])

input_file = None
with open(args.json, 'r') as f:
            input_file = json.load(f)

workbook = xlsxwriter.Workbook(args.excel)
worksheet = workbook.add_worksheet()
i = 0;
for s in {'time_created', 'source_time_created', 'source_file', 'input_file', 'input_sentence_number', 'complete', 'sentence_number'}:
    worksheet.write(i, 0, s)
    worksheet.write(i, 1, input_file[s])
    i += 1

r = 0;

for j in range(input_file['sentence_number']):
    for k in range(len(input_file["results"][j]['middles'])):
        entry = input_file["results"][j]
        middle = input_file["results"][j]['middles'][k]
        c = 3;
        for s in {"subject", "verb", "adverb"}:
            worksheet.write(r, c, middle[s]["index"])
            c += 1
            worksheet.write(r, c, middle[s]["word"])
            c += 1

        for s in {"sentence", "position"}:
            worksheet.write(r, c, entry[s])
            c += 1

        for s in {"note", "source_of_transitivity",}:
            worksheet.write(r, c, middle[s])
            c += 1
            
        r += 1

workbook.close()
